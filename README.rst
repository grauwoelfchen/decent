..
   RestructuredText: warning and note directives are not properly rendered #1682
   https://github.com/github/markup/issues/1682

.. note::
   This repository has been moved. Please check the new locations below.

   * `~grauwoelfchen/decent`_ on Sourcehut
   * `grauwoelfchen/decent`_ on Codeberg

.. _`~grauwoelfchen/decent`: https://git.sr.ht/~grauwoelfchen/decent
.. _`grauwoelfchen/decent`: https://codeberg.org/grauwoelfchen/decent


Decent
======

.. image:: https://gitlab.com/grauwoelfchen/decent/badges/trunk/pipeline.svg
   :target: https://gitlab.com/grauwoelfchen/decent/commits/trunk

.. image:: https://gitlab.com/grauwoelfchen/decent/badges/trunk/coverage.svg
   :target: https://gitlab.com/grauwoelfchen/decent/commits/trunk

An implementation of `libdecsync`_.

.. _`libdecsync`: https://github.com/39aldo39/libdecsync


+------------+--------------------+--------------+
| Name       | Crates.io          | Docs.rs      |
+============+====================+==============+
| decent     | |decent-crate|     | |decent-doc| |
+------------+--------------------+--------------+
| decent-cli | |decent-cli-crate| | N/A          |
+------------+--------------------+--------------+

.. |decent-crate| image:: https://img.shields.io/crates/v/decent?label=crates&style=flat
   :target: https://crates.io/crates/decent

.. |decent-doc| image:: https://img.shields.io/docsrs/decent?label=docs&style=flat
   :target: https://docs.rs/decent

.. |decent-cli-crate| image:: https://img.shields.io/crates/v/decent-cli?label=crates&style=flat
   :target: https://crates.io/crates/decent-cli


License
-------

``LGPL-3.0-or-later``

.. code:: text

   Decent
   Copyright (C) 2022-2025 Yasha
